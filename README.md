# pkgtree

This is a service which keep an index of packages and their dependencies. 
The service listens on TCP port `8080` for messages which provide the service with an instruction and parameters.

# Design

Messages from clients must follow this pattern:

```
<command>|<package>|<dependencies>\n
```

Where:
* `<command>` is mandatory, and is either `INDEX`, `REMOVE`, or `QUERY`
* `<package>` is mandatory, the name of the package referred to by the command, e.g. `mysql`, `openssl`, `pkg-config`, `postgresql`, etc.
* `<dependencies>` is optional, and if present it will be a comma-delimited list of packages that need to be present before `<package>` is installed. e.g. `cmake,sphinx-doc,xz`
* The message always ends with the character `\n`

## Index implementation
The package index is stored in memory as a built-in map so there's no external dependency.

To support multiple concurrent commands from different clients, the PackageIndex implements mutually exclusive locks using a sync.Mutex.
This prevents memory access collisions on the instance data. 

# Installation

There are three installation methods
* Using `go get`
* Downloading a pre-compiled binary
* Compiling the package yourself

## Using Go's `get` command
If your system has Go [installed and configured properly](https://golang.org/doc/install), run the following:
    ```
    go get gitlab.com/scotchneat/pkgtree
    ```

This will download the source and install the package to the $GOPATH/bin directory.

## Download a pre-compiled binary

1. Visit the package repository in a browser - https://gitlab.com/scotchneat/pkgtree.
1. Click the  
    ![Download](images/download-dropdown.png)
   
1. Then click the `compile` link on the bottom of `Download artifacts` section of the dropdown menu.
    ![compile](images/download-menu.png)
1. Make sure to use the appropriate binary for your OS/architecture.

## Compile the package yourself
    
1. Clone this repository to your $GOPATH/src/...`.
1. Run `make build` from the repository path.  
    > Note: This assumes your running the command from a system with the Make tool installed. 
    If you're not, examine the `build` target in the packages [Makefile](Makefile) to learn about the command. 
1. The compiled binary will be output to the `bin/` directory within the repository.


# Usage

Connecting to the service from a client can be verified using a network tool like [GNU NetCat](http://netcat.sourceforge.net/).
Once the tool is installed, run a command against the server like so:

1. Connect to the server
    ```
    nc localhost 8080
    ```

2. Send a message to index a package (the `\n` at the end of the message gets appended by pressing enter)
    ```
    INDEX|samplepackage|\n
    ```
    
    Here are more sample messages:
    ```
    INDEX|pkg1|\n
    INDEX|pkg2|pkg1\n
    REMOVE|cloog|\n
    QUERY|cloog|\n
    ```

