package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
	"testing"
)

const (
	testServerProtocol string = "tcp"
	testServerAddr     string = ":8888"
)

var processMessageTest = []struct {
	name     string
	msg      string
	expected ResponseCode
}{
	{"Invalid", "nothing", RError},
	{"IndexWithoutPackage", "INDEX||", RError},
	{"InvalidFormat", "INDEX|", RError},
	{"IndexNoDependencies", "INDEX|some|", RSuccess},
	{"IndexOverwriteDependencies", "INDEX|thing|some", RSuccess},
	{"IndexDependencyUnindexed", "INDEX|some|none", RFail},
	{"QueryIndexed", "QUERY|some|", RSuccess},
	{"QueryUnindexed", "QUERY|none|", RFail},
	{"RemoveIsDependency", "REMOVE|some|", RFail},
	{"RemoveIndexed", "REMOVE|thing|", RSuccess},
	{"RemoveUnindexed", "REMOVE|none|", RSuccess},
	{"RemoveAfterRemovingDependent", "REMOVE|some|", RSuccess},
}

func TestServer(t *testing.T) {
	// Temporary workaround for tests failing when run in a docker container
	if _, err := os.Stat("/.dockerenv"); err == nil {
		t.SkipNow()
	}

	go Server(testServerProtocol, testServerAddr)

	for _, i := range processMessageTest {
		t.Run(i.name, func(t *testing.T) {

			conn, err := net.Dial(testServerProtocol, testServerAddr)
			if err != nil {
				t.Error("Failed to connect to test server: ", err)
			}
			defer func() {
				if conn.Close() != nil {
					fmt.Println("Error closing connection")
				}
			}()

			msg := []byte(fmt.Sprintf("%s\n", i.msg))
			if _, err := conn.Write(msg); err != nil {
				t.Error("Failed to write message to server: ", err)
			}

			r := bufio.NewReader(conn)

			if actual, err := r.ReadString('\n'); err == nil {
				if strings.Compare(actual, string(i.expected)) != 0 {
					t.Errorf("Response [%q] does not match expected [%q].", actual, i.expected)
				}
			} else {
				t.Error("Failed to read message from server.")
			}
		})
	}
}
