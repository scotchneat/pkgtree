package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"
)

type LogType string
type ResponseCode string

const (
	LError LogType = "ERROR"
	LInfo  LogType = "INFO"

	RSuccess ResponseCode = "OK\n"
	RFail    ResponseCode = "FAIL\n"
	RError   ResponseCode = "ERROR\n"
)

func log(level LogType, source string, msg string) {
	fmt.Printf("%s\t%s\t%s\n", level, source, msg)
}

// Server is responsible or starting the TCP listener and accepting messages from connections
func Server(protocol string, addr string) {

	listener, err := net.Listen(protocol, addr)
	if err != nil {
		panic("Error starting listener.")
	}

	defer func() {
		if listener.Close() != nil {
			panic("Error closing listener.")
		}
	}()

	fmt.Printf("Package-Tree now listening on [%s] %s\n", protocol, addr)

	for {
		c, err := listener.Accept()
		if err != nil {
			fmt.Println(err)
			break
		}
		go handleConnection(c)
	}
}

// handleConnection reads messages from the client connection and sends a processed response
func handleConnection(c net.Conn) {
	clientAddr := c.RemoteAddr().String()
	log(LInfo, clientAddr, "connection established")
	defer func() {
		err := c.Close()
		if err != nil {
			log(LError, clientAddr, err.Error())
		}
	}()

	for {
		r := bufio.NewReader(c)
		msg, err := r.ReadString('\n')
		if err != nil {
			log(LInfo, clientAddr, "connection closed")
			break
		}

		sendResponse(c, processMessage(clientAddr, msg))

	}
}

// processMessage validates a string matches the expected format (<command>|<package>|<dependencies>\n)
// then processes the command and returns a respective ResponseCode
func processMessage(clientAddr string, msg string) ResponseCode {
	parts := strings.Split(msg, "|")

	if len(parts) != 3 {
		return RError
	}

	cmd := strings.ToUpper(parts[0])
	pkg := parts[1]

	if pkg == "" {
		return RError
	}

	depsString := strings.TrimSuffix(parts[2], "\n")
	var deps []string
	if len(depsString) > 0 {
		deps = strings.Split(depsString, ",")
	}

	if cmd == "INDEX" {
		if index.Index(pkg, deps) {
			log(LInfo, clientAddr, fmt.Sprintf("indexed `%s` package with dependencies %s", pkg, deps))
			return RSuccess
		}
	} else if cmd == "QUERY" {
		if index.Query(pkg) {
			return RSuccess
		}
	} else if cmd == "REMOVE" {
		if index.Remove(pkg) {
			log(LInfo, clientAddr, fmt.Sprintf("removed `%s` package with dependencies %s", pkg, deps))
			return RSuccess
		}
		log(LError, clientAddr, fmt.Sprintf("failed to remove `%s` package. check dependencies", pkg))
	} else {
		return RError
	}

	return RFail
}

// sendResponse writes the []byte representation of the ResponseCode to the client connection
func sendResponse(c net.Conn, code ResponseCode) {
	clientAddr := c.RemoteAddr().String()
	_, err := c.Write([]byte(code))
	if err != nil {
		log(LError, clientAddr, err.Error())
	}
}
