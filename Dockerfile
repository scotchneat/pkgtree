FROM golang:1.12-alpine as build
WORKDIR /go/src/gitlab.com/scotchneat/pkgtree
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GARCH=amd64 go build -v -o ./bin/pkgtree

FROM ubuntu
COPY --from=build /go/src/gitlab.com/scotchneat/pkgtree/bin/pkgtree /usr/local/bin/pkgtree
EXPOSE 8080
ENTRYPOINT ["/usr/local/bin/pkgtree"]