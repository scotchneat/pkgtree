BUILD_OUTPUT_PATH?=./bin
BUILD_SEPARATOR="********************"
BUILD_MESSAGE=echo "$(BUILD_SEPARATOR)\nBUILDING"
BUILD_COMMAND=GARCH=amd64 go build

help:  ## Prints this help message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}'

deps: ## Installs package dependencies
	@go get ./...

test: ## Runs all tests (with coverage analysis) for the package
	@go test -v -cover ./...

build: ## Builds the application for the current OS and architecture
	@go build -i -v .

build-all: build-darwin build-linux build-windows ## Builds the application for a linux OS and amd64 architecture

build-darwin:
	echo $(BUILD_OUTPUT_PATH) && \
	GOOS=darwin $(BUILD_COMMAND) -v -o $(BUILD_OUTPUT_PATH)/pkgtree_darwin .

build-linux:
	GOOS=linux $(BUILD_COMMAND) -v -o $(BUILD_OUTPUT_PATH)/pkgtree_linux .

build-windows:
	GOOS=windows $(BUILD_COMMAND)  -v -o $(BUILD_OUTPUT_PATH)/pkgtree_win .

docker: ## Builds the container image
	docker build --tag registry.gitlab.com/scotchneat/pkgtree:latest .

docker-run: docker ## Runs the latest docker image
	docker run -it --rm -p8080:8080 --name pkgtree registry.gitlab.com/scotchneat/pkgtree:latest

docker-test: ## Runs tests in a docker container
	docker run --rm -v$GOPATH/src/gitlab.com/scotchneat/pkgtree:/go/src/scotchneat/pkgtree -w/go/src/scotchneat/pkgtree -e CGO_ENABLED=0 golang:1.12-alpine go test -v -cover ./...

run: ## Runs the service
	@go run .

