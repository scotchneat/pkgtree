package main

import (
	"fmt"
	"sync"
	"testing"
)

var indexTests = []struct {
	name     string
	pkg      string
	deps     []string
	expected bool
}{
	{"NoDependencies", "one", []string{}, true},
	{"WithDependencies", "two", []string{"one"}, true},
	{"UnindexedDependencies", "three", []string{"fail"}, false},
	{"OverwritePackageDependencies", "one", []string{}, true},
}

var queryTests = []struct {
	name     string
	pkg      string
	expected bool
}{
	{"ExistingPackage", "one", true},
	{"ExistingPackage", "two", true},
	{"UnindexedPackage", "three", false},
	{"UnindexedPackage", "nope", false},
}

var removeTests = []struct {
	name     string
	pkg      string
	expected bool
}{
	{"PackageIsDependency", "one", false},
	{"IndependentPackage", "two", true},
	{"PackageNoLongerDependency", "one", true},
	{"PackageDoesNotExist", "nope", true},
}

func TestNewPackageIndex(t *testing.T) {

	var p interface{}
	p = NewPackageIndex()
	_, ok := p.(*PackageIndex)
	if ok != true {
		t.Fail()
	}
}

func TestPackageIndex(t *testing.T) {
	p := PackageIndex{
		mutex: sync.Mutex{},
		index: make(map[string][]string),
	}
	for _, i := range indexTests {
		testName := fmt.Sprintf("Index%s", i.name)
		t.Run(testName, func(t *testing.T) {
			actual := p.Index(i.pkg, i.deps)
			if actual != i.expected {
				t.Logf("TEST: %s, EXPECTED: %t, ACTUAL: %t", i.name, i.expected, actual)
				t.Fail()
			}
		})
	}

	for _, i := range queryTests {
		testName := fmt.Sprintf("Index%s", i.name)
		t.Run(testName, func(t *testing.T) {
			actual := p.Query(i.pkg)
			if actual != i.expected {
				t.Logf("TEST: %s, EXPECTED: %t, ACTUAL: %t", i.name, i.expected, actual)
				t.Fail()
			}
		})
	}

	for _, i := range removeTests {
		testName := fmt.Sprintf("Index%s", i.name)
		t.Run(testName, func(t *testing.T) {
			actual := p.Remove(i.pkg)
			if actual != i.expected {
				t.Logf("TEST: %s, EXPECTED: %t, ACTUAL: %t", i.name, i.expected, actual)
				t.Fail()
			}
		})
	}
}
