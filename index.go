package main

import "sync"

type Index interface {
	Index(name string, deps []string) bool
	Query(name string) bool
	Remove(name string) bool
}

type PackageIndex struct {
	mutex sync.Mutex
	index map[string][]string
}

// NewPackageIndex is the factory method which creates an instance of the PackageIndex
func NewPackageIndex() *PackageIndex {
	return &PackageIndex{
		mutex: sync.Mutex{},
		index: make(map[string][]string),
	}
}

// Index either saves or updates a package in the index.
// The `deps` argument is optional.
// When an index is updated, the dependencies are overwritten by the new values.
// Returns `true` if the command is successful.
func (p *PackageIndex) Index(name string, deps []string) bool {
	for _, dep := range deps {
		if !p.Query(dep) {
			return false
		}
	}

	p.mutex.Lock()
	p.index[name] = deps
	p.mutex.Unlock()

	return true
}

// Query checks if a package is already indexed and returns `true`
func (p *PackageIndex) Query(name string) bool {
	p.mutex.Lock()
	_, exists := p.index[name]
	p.mutex.Unlock()

	return exists
}

// Remove deletes a package from the index.
// Returns `true` when successful.
// Will also return `true` if the package wasn't in the index.
func (p *PackageIndex) Remove(name string) bool {
	if !p.Query(name) {
		return true
	}

	if !p.isDependency(name) {
		p.mutex.Lock()
		delete(p.index, name)
		p.mutex.Unlock()
	}
	return !p.Query(name)
}

// isDependency searches the dependencies of all indexed packages for a package name.
// Returns `true` if the given package name is found.
func (p *PackageIndex) isDependency(name string) bool {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	for _, deps := range p.index {
		for _, dep := range deps {
			if dep == name {
				return true
			}
		}
	}
	return false
}
