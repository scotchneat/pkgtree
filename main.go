// pkgtree starts a TCP listener on port 8080
package main

var index = NewPackageIndex()

func main() {
	Server("tcp", ":8080")
}
